<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Accordion extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'always_open' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
        ]);
    }

    protected function baseClass()
    {
        return 'accordion';
    }
}
