<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Modal extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'title' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'size' => (object) [
                'type' => 'enum:sm,none,lg,xl,fullscreen|array',
                'default' => 'none',
            ],
            'scrollable' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'buttons' => (object) [
                'type' => 'array',
                'default' => [
                    ['color' => 'secondary', 'label' => 'Close', 'attributes' => ['data-bs-dismiss' => 'modal']]
                ]
            ],
        ]);
    }

    protected function localAttributes()
    {
        return array_merge(parent::localAttributes(), ['buttons']);
    }

    protected function baseClass()
    {
        return 'modal fade';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        $classes = [];

        if (is_array($params['size'])) {
            $classes[] = join(' ', $params['size']);
        }
        else {
            $classes[] = sprintf('modal-%s', $params['size']);
        }

        if ($params['scrollable']) {
            $classes[] = 'modal-dialog-scrollable';
        }

        $params['generated_class_dialog'] = join(' ', $classes);

        return $params;
    }
}
