<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\TranslateModels;
use MadBob\Larastrap\Base\ObjectOptions;

class ChecksModel extends Checks
{
    use TranslateModels;
    use ObjectOptions;

    public static function parameters()
    {
        return array_merge(parent::parameters(), self::translatingParameters());
    }

    public function templateName()
    {
        return 'checks';
    }

    protected function processParams($params)
    {
        $params = $this->fixOptions($params);
        $params = parent::processParams($params);
        return $this->translateOptions($params);
    }
}
