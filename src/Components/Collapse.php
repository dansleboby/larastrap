<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Container;

class Collapse extends Container
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'open' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
        ]);
    }

    protected function baseClass()
    {
        return 'collapse';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        if ($params['open']) {
            $params['classes'] = array_merge($params['classes'] ?? [], ['show']);
        }

        return $params;
    }
}
