<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\TranslateModels;
use MadBob\Larastrap\Base\ObjectOptions;

class RadiolistModel extends Radiolist
{
    use TranslateModels;
    use ObjectOptions;

    public static function parameters()
    {
        return array_merge(parent::parameters(), self::translatingParameters());
    }

    public function templateName()
    {
        return 'radiolist';
    }

    protected function processParams($params)
    {
        $params = $this->fixOptions($params);
        $params = parent::processParams($params);
        return $this->translateOptions($params);
    }
}
