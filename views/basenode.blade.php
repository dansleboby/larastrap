<{{ $params['node'] }} id="{{ $params['id'] }}" class="{{ $params['generated_class'] }}" {!! $params['serialized_attributes'] !!}>{{ $slot }}</{{ $params['node'] }}>
