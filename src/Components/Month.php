<?php

namespace MadBob\Larastrap\Components;

class Month extends Input
{
    protected function inputType()
    {
        return 'month';
    }
}
