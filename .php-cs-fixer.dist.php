<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__)
    ->exclude('vendor')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
	'@PSR12' => true,
	'control_structure_continuation_position' => false,
])->setFinder($finder);
