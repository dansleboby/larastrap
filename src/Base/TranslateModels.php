<?php

namespace MadBob\Larastrap\Base;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

trait TranslateModels
{
    protected static function translatingParameters()
    {
        return [
            'extra_options' => (object) [
                'type' => 'array',
                'default' => [],
            ],
            'translateCallback' => (object) [
                'type' => 'callable',
                'default' => function ($obj) {
                    return [$obj->id, $obj->name];
                },
            ],
        ];
    }

    private function translateOption($optkey, $option, $params)
    {
        if (is_a($option, Model::class)) {
            list($id, $value) = call_user_func($params['translateCallback'], $option);
        }
        else {
            $id = $optkey;
            $value = $option;
        }

        return [$id, $value];
    }

    public function translateOptions($params)
    {
        $translated = [];

        $extraitem = $params['extra_options'] ?? false;
        if ($extraitem) {
            if (is_array($extraitem)) {
                $translated = $extraitem;
            }
            else {
                $translated['0'] = $extraitem;
            }

            unset($params['extra_options']);
        }

        try {
            foreach ($params['options'] as $optkey => $option) {
                list($id, $value) = $this->translateOption($optkey, $option, $params);
                $translated[$id] = $value;
            }
        }
        catch(\Exception $e) {
            dd($params['options']);
            exit();
        }

        $params['options'] = $translated;

        $translated_value = [];

        if ($params['value']) {
            if (is_array($params['value']) || is_a($params['value'], Collection::class)) {
                foreach($params['value'] as $val) {
                    list($id, $value) = $this->translateOption(null, $val, $params);
                    $translated_value[] = $id;
                }
            }
            else {
                list($id, $value) = $this->translateOption(null, $params['value'], $params);
                $translated_value = [$id];
            }
        }

        if (empty($translated_value) && $extraitem) {
            $keys = array_keys($translated);
            sort($keys);
            foreach ($keys as $k) {
                $translated_value[] = $k;
                break;
            }
        }

        $params['value'] = $translated_value;

        return $params;
    }
}
