<x-larastrap::field :params="$params['field_params']">
    <div class="form-check {{ $params['switch'] ? 'form-switch' : '' }} mt-1">
        <input id="{{ $params['id'] }}" type="checkbox" class="{{ $params['generated_class'] }}" name="{{ $params['actualname'] }}" {!! filled($params['value']) ? sprintf('value="%s"', $params['value'])  : '' !!} {!! $params['serialized_attributes'] !!}>

        @if($params['inline'])
            <x-larastrap::label :params="$params['field_params']['label_params']" />
        @endif

        @include('larastrap::partials.error', ['params' => $params])
    </div>
</x-larastrap::field>
