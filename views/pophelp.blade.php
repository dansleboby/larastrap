<span class="{{ $params['generated_class'] }}" data-bs-toggle="popover" data-bs-trigger="hover" data-bs-content="{!! html_entity_decode($params['text']) !!}">{!! $params['symbol'] !!}</span>
