<?php

namespace MadBob\Larastrap\Components;

class Text extends Input
{
    protected function inputType()
    {
        return 'text';
    }
}
