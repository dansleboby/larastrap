<x-larastrap::form>
    <x-larastrap::hidden name="hide" value="1" />
    <x-larastrap::text name="name" label="A Text" />
    <x-larastrap::textarea name="longtext" label="A Textarea" />
    <x-larastrap::number name="numeric" label="A Number" />
    <x-larastrap::email name="email" label="An Email" />
    <x-larastrap::url name="address" label="An URL" />
    <x-larastrap::range name="value" label="A Range" />
    <x-larastrap::date name="day" label="A Date" />
    <x-larastrap::time name="hour" label="A Time" />
    <x-larastrap::datetime name="dayhour" label="Both Date and Time" />
    <x-larastrap::color name="tint" label="A Color" />
    <x-larastrap::check name="boolean" label="A Boolean" />
    <x-larastrap::radios name="single" label="Choose One" :options="['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue']" />
    <x-larastrap::checks name="multi" label="Choose Many" :options="['red' => 'Red', 'green' => 'Green', 'blue' => 'Blue']" />
    <x-larastrap::select name="selection" label="Selection" :options="['first' => 'First', 'second' => 'Second', 'third' => 'Third']" />
</x-larastrap::form>
