<?php

use Orchestra\Testbench\Concerns\WithWorkbench;
use Illuminate\Support\ViewErrorBag;

class BaseTest extends \Orchestra\Testbench\TestCase
{
    use WithWorkbench;

    protected function setUp(): void
    {
        parent::setUp();

        /*
            Errors are usually pushed into the View by Sessions or exception
            handlers, but none of them are used in the basic non-HTTP tests.
            We need to artifically init them, to avoid errors in the error
            handling templates (e.g. input)
        */
        view()->share('errors', new ViewErrorBag());
    }

    private function checkExistance($html, $rules)
    {
        $doc = new \DOMDocument();
        $doc->loadHTML($html);
        $xpath = new \DOMXpath($doc);

        foreach($rules as $rule => $quantity) {
            $elements = $xpath->query($rule);

            if ($elements->count() != $quantity) {
                echo $html;
                echo "\n\n";
                echo $rule;
                $this->fail();
            }
            else {
                $this->assertTrue(true);
            }
        }
    }

    public function testBaseForm()
    {
        $html = view('baseform')->render();

        $this->checkExistance($html, [
            "//form/div/div/input[@type='text'][@name='name']" => 1,
            "//form/input[@type='hidden'][@name='hide']" => 1,
            "//form/div/div/input[@type='email'][@name='email']" => 1,
            "//form/div/div/div/input[@name='single']" => 3,
            "//form/div/div/div/input[@name='multi[]']" => 3,
            "//form/div/div/select/option[@value='first']" => 1,
            "//form/*/button[@type='submit']" => 1,
        ]);
    }

    public function testBaseModalForm()
    {
        $html = view('basemodalform')->render();

        $this->checkExistance($html, [
            "//div[contains(@class, 'modal')]/div[contains(@class, 'modal-body')]/form/div/div/input[@name='name']" => 1,
            "//div[contains(@class, 'modal')]/div[contains(@class, 'modal-footer')]/button[@type='submit']" => 1,
        ]);
    }

    public function testComplexAttributes()
    {
        $html = view('complexattributes')->render();

        $this->checkExistance($html, [
            "//form/div[@hidden]/div/input[@type='text' and @name='hide']" => 1,
            "//form/div/div/textarea[@rows='20']" => 1,
            "//form/div/div/input[@type='url'][@name='address' and @data-foo='bar']" => 1,
        ]);

        $doc = new \DOMDocument();
        $doc->loadHTML($html);
        $xpath = new \DOMXpath($doc);

        $elements = $xpath->query("//form/div/div/input[@type='text'][@name='bool1']");
        $this->assertEquals(1, $elements->count());
        foreach($elements as $el) {
            $this->assertNull($el->attributes->getNamedItem('foobar'));
        }

        $elements = $xpath->query("//form/div/div/input[@type='text'][@name='bool2']");
        $this->assertEquals(1, $elements->count());
        foreach($elements as $el) {
            $this->assertNotNull($el->attributes->getNamedItem('foobar'));
        }

        $elements = $xpath->query("//form/div/div/input[@type='text'][@name='bool3']");
        $this->assertEquals(1, $elements->count());
        foreach($elements as $el) {
            $this->assertNotNull($el->attributes->getNamedItem('barbaz'));
        }
    }
}
