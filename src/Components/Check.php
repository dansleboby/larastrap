<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Commons;

class Check extends Input
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'checked' => (object) [
                'type' => 'boolean',
                'default' => null,
                'serialize' => true,
            ],
            'switch' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'inline' => (object) [
                'type' => 'boolean',
                'default' => false,
            ],
            'triggers_collapse' => (object) [
                'type' => 'string',
                'default' => '',
            ],
        ]);
    }

    protected function baseClass()
    {
        return 'form-check-input';
    }

    protected function inputType()
    {
        return 'checkbox';
    }

    public function templateName()
    {
        return 'check';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        $params = Commons::processParamsAsInput($this, $params);

        if ($params['checked'] === null && (is_bool($params['value']) || ($params['value'] === 0 || $params['value'] === 1))) {
            $params['checked'] = $params['value'];
        }

        if ($params['inline']) {
            if ($params['formview'] == 'vertical') {
                $params['field_params']['do_label'] = 'hide';
            }
            else {
                $params['field_params']['do_label'] = 'empty';
            }
        }

        return $params;
    }

    public function retrieveOldValue($params)
    {
        $old = old($params['name']);

        if (is_null($old) == false) {
            $params['checked'] = true;
            $params['value_from_object'] = false;
        }

        return $params;
    }

    protected function finalProcess($params)
    {
        if (filled($params['triggers_collapse'])) {
            $params['attributes']['data-bs-toggle'] = 'collapse';
            $params['attributes']['data-bs-target'] = Commons::prefixId($params['triggers_collapse']);

            if ($params['checked']) {
                $id = Commons::unprefixId($params['triggers_collapse']);
                $this->getStack()->setStatusByID($id, ['open' => true]);
            }
        }

        return parent::finalProcess($params);
    }
}
