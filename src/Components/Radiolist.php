<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;
use MadBob\Larastrap\Base\Commons;

class Radiolist extends Element
{
    public static function parameters()
    {
        return array_merge(Input::parameters(), [
            'options' => (object) [
                'type' => 'index_array',
                'default' => [],
            ],
            'label_class' => (object) [
                'type' => 'string_array',
                'default' => [],
            ],
        ]);
    }

    protected function exposedMethods()
    {
        return ['parseOption'];
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        $params = Commons::processParamsAsInput($this, $params);

        if ($params['required'] && empty($params['value']) && count($params['options']) == 1) {
            $params['value'] = array_keys($params['options'])[0];
        }

        return $params;
    }

    protected function localAttributes()
    {
        return array_merge(parent::localAttributes(), ['label_class']);
    }

    public function parseOption($option, $params)
    {
        $css_classes = [
            'form-check-label',
        ];

        if (is_string($option)) {
            $ret = (object) [
                'id' => '',
                'hidden' => false,
                'label' => $option,
                'disabled' => false,
                'classes' => [],
                'serialized_label_attributes' => [],
            ];
        }
        else {
            $ret = (object) [
                'id' => $option->id ?? false,
                'hidden' => $option->hidden ?? false,
                'label' => $option->label ?? '',
                'disabled' => $option->disabled ?? false,
                'classes' => $option->label_classes ?? [],
                'serialized_label_attributes' => $option->label_attributes ?? [],
            ];
        }

        if ($ret->disabled) {
            $ret->serialized_label_attributes[] = 'disabled';
        }

        if (empty($ret->id)) {
            $ret->id = $params['id'] . '-' . md5($ret->label);
        }

        $ret->serialized_label_attributes = Commons::serializeAttributes($option->label_attributes ?? []);

        if (!empty($ret->classes)) {
            $css_classes = array_merge($css_classes, $ret->classes);
        }

        if (!empty($params['label_class'])) {
            $css_classes = array_merge($css_classes, $params['label_class']);
        }

        $ret->serialized_label_classes = join(' ', $css_classes);

        return $ret;
    }
}
