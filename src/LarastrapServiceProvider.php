<?php

namespace MadBob\Larastrap;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

use MadBob\Larastrap\Base\Stack;

class LarastrapServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/larastrap.php',
            'larastrap'
        );

        $this->app->singleton('LarastrapStack', function ($app) {
            return new Stack();
        });
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'larastrap');

        $this->publishes([
            __DIR__ . '/../config/larastrap.php' => config_path('larastrap.php')
        ], 'config');

        Blade::componentNamespace('MadBob\\Larastrap\\Components', 'larastrap');

        $stack = app()->make('LarastrapStack');
        $customs = config('larastrap.customs');
        if ($customs) {
            foreach($customs as $tag => $config) {
                $stack->addCustomElement($tag, $config);
            }
        }
    }
}
