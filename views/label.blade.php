<label for="{{ $params['for'] }}" class="{{ $params['generated_class'] }}">
    {!! $params['label'] !!}

    @if(filled($params['pophelp']))
        <x-larastrap::pophelp :text="$params['pophelp']" />
    @endif
</label>
