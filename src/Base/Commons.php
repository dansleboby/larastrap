<?php

namespace MadBob\Larastrap\Base;

use Illuminate\Support\Str;
use Illuminate\Support\Collection;

use MadBob\Larastrap\Components\Radios;
use MadBob\Larastrap\Components\Form;

class Commons
{
    public static function normalizeParams($params, $config)
    {
        foreach($params as $key => $value) {
            if (!isset($config[$key])) {
                continue;
            }

            $conf = $config[$key];

            switch($conf->type) {
                case 'boolean':
                    $params[$key] = filter_var($params[$key], FILTER_VALIDATE_BOOLEAN);
                    break;

                case 'string_array':
                    if (is_string($params[$key])) {
                        $params[$key] = explode(' ', $params[$key]);
                    }
                    break;

                case 'html_version':
                    if (is_string($params[$key]) && filled($params[$key])) {
                        $params[$conf->to] = html_entity_decode($params[$key]);
                    }
                    break;
            }
        }

        return $params;
    }

    public static function convertQueryStringToDotNotation($name)
    {
        if(strpos($name, '[') !== false && strpos($name, ']') !== false  && substr($name, -2) !== '[]') {
            parse_str($name, $parseParams);
            $dot = \Arr::dot($parseParams);
            return array_keys($dot)[0];
        }

        return $name;
    }

    public static function retrieveText($params, $explicit = 'label', $translatable = 'tlabel')
    {
        $default = $params[$explicit] ?? '';

        if (blank($default)) {
            $identifier = $params[$translatable] ?? '';

            if (blank($identifier)) {
                $default = '';
            }
            else {
                $default = __($identifier);
            }
        }

        $params[$explicit] = $default;
        return $params;
    }

    /*
        Many input components are not directly extending the Input class, but
        still have the same behaviour when processing parameters. The function
        is shared here for convenience
    */
    public static function processParamsAsInput($component, $params)
    {
        $stack = app()->make('LarastrapStack');

        if ($params['readonly'] || $params['disabled']) {
            if ($params['asplaintext']) {
                $params['classes'][] = 'form-control-plaintext';

                if (($key = array_search('form-control', $params['classes'])) !== false) {
                    unset($params['classes'][$key]);
                }
            }
        }

        $parent = $component->closestParent(Form::class);
        if ($parent) {
            $parent_status = $stack->getStatus($parent, false);

            if ($parent_status['formview'] == 'vertical') {
                /*
                    When a Form has a vertical view, the inner radios (and
                    checks) have to be displayed as block to keep the general
                    alignment
                */
                if (is_a($component, Radios::class)) {
                    $params['classes'][] = 'd-block';
                }
            }
            elseif ($parent_status['formview'] == 'inline') {
                if (blank($params['placeholder'])) {
                    $params['placeholder'] = $params['label'];
                }
            }
        }

        $value = $params['value'] ?? null;
        $params['value_from_object'] = false;

        if ($value === null) {
            $params['value'] = $stack->getValue($params['name']);
            $params['value_from_object'] = true;
        }

        $params['actualname'] = $params['nprefix'] . $params['name'] . $params['npostfix'];
        if (blank($params['actualname'])) {
            $params['actualname'] = Str::random(10);
        }

        $params['dotname'] = self::convertQueryStringToDotNotation($params['name']);

        if ($params['error_handling']) {
            if (request()->hasSession()) {
                $session = request()->session();
                if ($session) {
                    $errors = $session->get('errors');
                    if ($errors) {
                        $bag = $errors->getBag($params['error_bag']);
                        if ($bag && $bag->isEmpty() == false) {
                            if ($bag->has($params['dotname'])) {
                                $params['classes'][] = 'is-invalid';
                            }
                            $params = $component->retrieveOldValue($params);
                        }
                    }
                }
            }
        }

        /*
            Here I duplicates parameters for the label, both in the main array
            and in a specific sub-array "label_params" (as the internal code for
            Field element does). This is to manage the usual case (an input
            wrapped by a Field, with his params) and the special case of inline
            checkboxes (when the label is handled directly by the element).
            TODO: this is largely to be reviewed
        */
        $params['field_params'] = [
            'classes' => [],
            'id' => $params['id'],
            'do_label' => $params['do_label'],
            'help' => $params['help'],
            'squeeze' => $params['squeeze'],
            'hidden' => $params['hidden'],
            'label' => $params['label'],
            'label_class' => $params['label_class'],
            'pophelp' => $params['pophelp'],
            'label_params' => [
                'label' => $params['label'],
                'label_class' => $params['label_class'],
                'pophelp' => $params['pophelp'],
                'for' => $params['id'],
            ]
        ];

        return $params;
    }

    public static function processParamsMultipleValues($params)
    {
        if (is_a($params['value'], Collection::class)) {
            $actual_value = [];

            foreach($params['value'] as $val) {
                $actual_value[] = $val;
            }

            $params['value'] = $actual_value;
        }

        if (is_array($params['value']) === false) {
            $params['value'] = [];
        }

        return $params;
    }

    public static function serializeAttributes($attributes, $serialized = [])
    {
        foreach($attributes as $attr_name => $attr_value) {
            try {
                if ($attr_name == 'params') {
                    $serialized[] = self::serializeAttributes($attr_value, $serialized);
                }
                else {
                    /*
                        When an attribute has specific boolean value, it is
                        assumed it is an activable/deactivable flag an is
                        serialized into the DOM when actually true.
                        This is to permit use of arbitrary HTML attributes, even
                        not natively managed by Larastrap, triggerable through
                        some boolean expression
                    */
                    if (is_bool($attr_value)) {
                        if ($attr_value == false) {
                            continue;
                        }
                        else {
                            $attr_value = $attr_name;
                        }
                    }

                    $serialized[] = sprintf('%s="%s"', $attr_name, $attr_value);
                }
            }
            catch(\Exception $e) {
                throw new \Exception('Invalid attribute: ' . $attr_name);
            }
        }

        if (!empty($serialized)) {
            return join(' ', $serialized);
        }
        else {
            return '';
        }
    }

    public static function prefixId($id)
    {
        $prefix = '';
        if (Str::startsWith($id, '#') == false) {
            $prefix = '#';
        }

        return $prefix . $id;
    }

    public static function unprefixId($id)
    {
        if (Str::startsWith($id, '#')) {
            return substr($id, 1);
        }
        else {
            return $id;
        }
    }
}
