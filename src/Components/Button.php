<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;
use MadBob\Larastrap\Base\Commons;

class Button extends Element
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'color' => (object) [
                'type' => 'string',
                'default' => 'primary',
            ],
            'label' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'label_html' => (object) [
                'type' => 'html_version',
                'default' => '',
                'to' => 'label',
            ],
            'tlabel' => (object) [
                'type' => 'string',
                'translates' => 'label',
                'default' => '',
            ],
            'prelabel' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'postlabel' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'title' => (object) [
                'type' => 'string',
                'default' => '',
                'serialize' => true,
            ],
            'ttitle' => (object) [
                'type' => 'string',
                'translates' => 'title',
                'default' => '',
            ],
            'size' => (object) [
                'type' => 'enum:sm,none,lg',
                'default' => 'none',
            ],
            'triggers_modal' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'triggers_collapse' => (object) [
                'type' => 'string',
                'default' => '',
            ],
        ]);
    }

    protected function baseClass()
    {
        return 'btn';
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);

        if (empty($params['override_classes'])) {
            $params['classes'][] = 'btn-' . $params['color'];

            if ($params['size'] != 'none') {
                $params['classes'][] = 'btn-' . $params['size'];
            }
        }

        if (filled($params['triggers_modal'])) {
            $params['attributes']['data-bs-toggle'] = 'modal';
            $params['attributes']['data-bs-target'] = Commons::prefixId($params['triggers_modal']);
        }

        if (filled($params['triggers_collapse'])) {
            $params['attributes']['data-bs-toggle'] = 'collapse';
            $params['attributes']['data-bs-target'] = Commons::prefixId($params['triggers_collapse']);
        }

        $params['label'] = trim(sprintf('%s%s%s', $params['prelabel'], $params['label'], $params['postlabel']));

        return $params;
    }
}
