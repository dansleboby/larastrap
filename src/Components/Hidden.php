<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;
use MadBob\Larastrap\Base\Commons;

class Hidden extends Element
{
    public static function parameters()
    {
        return Input::parameters();
    }

    protected function processParams($params)
    {
        $params = parent::processParams($params);
        return Commons::processParamsAsInput($this, $params);
    }
}
