<?php

namespace MadBob\Larastrap\Components;

class Number extends Input
{
    protected function inputType()
    {
        return 'number';
    }
}
