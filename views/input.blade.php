@php

$is_group = filled($params['textappend']) || filled($params['textprepend']);

/*
    Usually the "hidden" attribute is destinated to the wrapping Field, but when
    the "squeeze" behavior is also required the parent Field is omitted. So the
    Input field rendered in this template needs to be directly hidden
*/
$to_hide = $params['hidden'] && $params['squeeze'];

@endphp

<x-larastrap::field :params="$params['field_params']">
    @if($is_group)
    <div class="input-group {{ in_array('is-invalid', $params['classes']) ? 'has-validation' : '' }}" {{ $to_hide ? 'hidden' : '' }}>
    @endif

    @if(filled($params['textprepend']))
        <span class="input-group-text">{!! $params['textprepend'] !!}</span>
    @endif

        <input id="{{ $params['id'] }}" type="{{ $inputtype }}" class="{{ $params['generated_class'] }}" name="{{ $params['actualname'] }}" value="{{ $params['value'] }}" {!! $params['serialized_attributes'] !!}>

    @if(filled($params['textappend']))
        <span class="input-group-text">{!! $params['textappend'] !!}</span>
    @endif

    @if($is_group)
    </div>
    @endif

    @include('larastrap::partials.error', ['params' => $params])
</x-larastrap::field>
