<div id="{{ $params['id'] }}" class="{{ $params['generated_class'] }}" {!! $params['serialized_attributes'] !!}>
    <div class="card card-body">
        {{ $slot }}
        @include('larastrap::appended_nodes', ['params' => $params])
    </div>
</div>
