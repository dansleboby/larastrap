<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;
use MadBob\Larastrap\Base\Commons;
use MadBob\Larastrap\Base\ObjectOptions;

class Select extends Element
{
    use ObjectOptions;

    public static function parameters()
    {
        return array_merge(Input::parameters(), [
            'options' => (object) [
                'type' => 'array|callable',
                'default' => [],
            ],
            'multiple' => (object) [
                'type' => 'boolean',
                'default' => false,
                'serialize' => true,
            ],
        ]);
    }

    protected function exposedMethods()
    {
        return ['unrollOptions'];
    }

    protected function processParams($params)
    {
        $params = $this->fixOptions($params);
        $params = parent::processParams($params);

        /*
            This is to enforce handling options as an HTML array
        */
        if ($params['multiple'] && empty($params['npostfix'])) {
            $params['npostfix'] = '[]';
        }

        $params = Commons::processParamsAsInput($this, $params);
        return $params;
    }

    protected function baseClass()
    {
        return 'form-select';
    }

    public function unrollOptions($options, $selected_value)
    {
        $ret = '';

        foreach($options as $option_value => $option) {
            if (is_object($option)) {
                $label = $option->label;
                $disabled = $option->disabled ?? false;

                if (isset($option->children) && is_array($option->children)) {
                    $ret .= sprintf('<optgroup label="%s">', $label ?? '');
                    $ret .= $this->unrollOptions($option->children, $selected_value);
                    $ret .= sprintf('</optgroup>');
                    continue;
                }
            }
            else {
                $label = $option;
                $disabled = false;
            }

            $attributes = [];
            if ($disabled) {
                $attributes[] = 'disabled';
            }

            /*
                Warning: here it is not enforced a comparison for the same
                datatype. Mostly for convenience, but may lead to errors
            */
            if (is_array($selected_value)) {
                $selected = in_array($option_value, $selected_value);
            }
            else {
                $selected = ((string) $selected_value == $option_value);
            }

            if ($selected) {
                $attributes[] = 'selected';
            }

            $ret .= sprintf('<option value="%s" %s>%s</option>', $option_value, join(' ', $attributes), $label);
        }

        return $ret;
    }
}
