<?php

namespace MadBob\Larastrap\Components;

use MadBob\Larastrap\Base\Element;

class Pophelp extends Element
{
    public static function parameters()
    {
        return array_merge(parent::parameters(), [
            'text' => (object) [
                'type' => 'string',
                'default' => '',
            ],
            'ttext' => (object) [
                'type' => 'string',
                'translates' => 'text',
                'default' => '',
            ],
            'symbol' => (object) [
                'type' => 'string',
                'default' => '?',
            ],
            'symbol_html' => (object) [
                'type' => 'html_version',
                'default' => '',
                'to' => 'symbol',
            ],
        ]);
    }

    protected function baseClass()
    {
        return 'badge rounded-pill bg-primary';
    }
}
